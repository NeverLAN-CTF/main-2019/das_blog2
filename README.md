# 2019_das_blog2

#### Description
A sql injection challenge where you need to find the user with admin permissions

#### Flag
flag{Pwn3d_W1th_SQL}

#### Hint
Try the basic injection technic where permissions='admin'
