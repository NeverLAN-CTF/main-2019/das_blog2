<?php
require_once ('inc/common.php');
$output='';
if( array_key_exists('Username', $_POST) && array_key_exists('Password', $_POST) ){
	// never use unescaped data
    $name = $_POST['Username'];
	// in a real environment a better hashing algorithum should be used
	$pass = hash('sha256', $_POST['Password']);
    $sql = "SELECT `name`, `permissions` FROM `users` WHERE `name`='$name' AND `password`='$pass' LIMIT 1;";
    if( $data = mysqli_query($conn, $sql) ){
        if( mysqli_num_rows($data) > 0){
            $result = mysqli_fetch_assoc($data);
            $_SESSION['username'] = $result['name'];
            $_SESSION['permissions'] = $result['permissions'];
            $output = 'You are now logged in as '.$result['name'].' with permissions '.$result['permissions'];
			$output .= '</br><a href="/"> click here to go to the main page</a>';
        }else{
            $output = 'Sorry, That Username / Password is incorrect.';
        }
    }else{
        $output = 'There was a problem when looking for user. Please try again.';
    }
}else if(array_key_exists('logout', $_POST) ){
    setcookie('user', '', time() - 3600); // set timeout to yesterday to remove cookie
    setcookie('permissions', '', time() - 3600);
    $output = 'You have been logged out.';
}

// login user, set session data in cookie
// output comment with test user info, test user only has basic permissions

 ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Das Blog Login page</title>
    </head>
    <body>
        <p><?php echo($output?$output:'Please login here');?></p>
        <form action="?" method="post">
            <label for="Username">Username</label>
            <input type="text" name="Username" />
            <br>
            <label for="Password">Password</label>
            <input type="password" name="Password" />
            <br>
            <input type="submit" name="submit" value="Login"/>
        </form>
    </body>
</html>

